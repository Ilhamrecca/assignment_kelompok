// Importing Module
const { Console } = require("console");
const { truncate } = require("fs");
const readline = require("readline");
const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

function menu() {
	console.log("Calculate 3D Volume\n");

	console.log("Select Which 3D Shape you would like to calculate");
	console.log("1. Cube \n2. Tube");
	rl.question(
		"Please select between 1 or 2, Select either of those to close the program: ",
		(choice) => {
			if (validateInput(choice)) {
				choosenOption(choice);
			} else {
				console.log("\n\nPlease Enter a Number\n");
				menu();
			}
		}
	);
}

function choosenOption(choice) {
	if (choice != 1 && choice != 2) {
		console.log("Good bye!\n\n");
		rl.close();
	} else {
		if (choice == 1) {
			rl.question("What's the side length of the cube ? ", (length) => {
				if (validateInput(length)) {
					calculateCube(length);
				} else {
					console.log("\n\nPlease Enter a Number\n");
					choosenOption(choice);
				}
			});
		} else {
			rl.question("What's the side Radius of the Tube ? ", (radius) => {
				rl.question("Whats the height of the Tube ", (height) => {
					if (validateInput(radius) && validateInput(height)) {
						calculateTube(radius, height);
					} else {
						console.log("\n\nPlease Enter a Number\n");
						choosenOption(choice);
					}
				});
			});
		}
	}
}

function calculateCube(length) {
	console.log(`\n\nThe Volume of the cube is ${length ** 3} Unit Volume`);
	menu();
}

function calculateTube(radius, height) {
	console.log(
		`\n\nThe Volume of the Tube is ${
			3.14 * radius ** 2 * height
		} Unit Volume`
	);
	menu();
}

function validateInput(input) {
	// switch (type) {
	// 	case "number":
	// 		return isNaN(input) ? false : true;
	// 	case "string":
	// 		return typeof input === "string" ? true : false;
	// }

	if (isNaN(Number(input)) || input === "") {
		return false;
	} else {
		return true;
	}
}

rl.on("close", () => {
	process.exit();
});

menu();
