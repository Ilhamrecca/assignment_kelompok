console.clear();
const vegetable = ["Tomato", "Broccoli", "Kale", "Cabbage", "Apple"];
for (let i = 0; i < vegetable.length; i++) {
	vegetable[i] === "apple"
		? console.log("Apple is not vegetable")
		: console.log(
				`${vegetable[i]} is a healthy food, it's definitely worth to eat.`
		  );
}
