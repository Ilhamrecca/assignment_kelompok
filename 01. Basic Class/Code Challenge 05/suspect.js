const { Console } = require("console");
const { truncate } = require("fs");
const readline = require("readline");
const { Z_FILTERED } = require("zlib");

const data = [
	{
		name: "John",
		status: "Positive",
	},
	{
		name: "Mike",
		status: "Suspect",
	},
	{
		name: "Luke",
		status: "Suspect",
	},
	{
		name: "James",
		status: "Positive",
	},
	{
		name: "Linus",
		status: "Positive",
	},
	{
		name: "Jack",
		status: "Positive",
	},
	{
		name: "Anthony",
		status: "Suspect",
	},
];

const menu = function () {
	rl.question("1. Suspect \n2. Positive \nFilter by : ", (choice) => {
		switch (choice) {
			case "1":
				console.clear();
				search("Suspect");
				menu();
				break;
			case "2":
				console.clear();
				search("Positive");
				menu();
				break;
			default:
				console.log("Pilih antara 1 atau 2");
				menu();
		}
	});
};

const search = function (keyword) {
	for (let i = 0; i < data.length; i++) {
		if (data[i].status === keyword) console.log(`${data[i].name}`);
	}
};

const rl = readline.createInterface({
	input: process.stdin,
	output: process.stdout,
});

rl.on("close", () => {
	process.exit();
});

menu();
